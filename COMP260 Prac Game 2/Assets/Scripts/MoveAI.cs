﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class MoveAI : MonoBehaviour {

	private Rigidbody rigidbody;
	public Rigidbody puck1;
	public Transform defaultAIPos;

	public float speed;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false;
		PuckControl puckStore = (PuckControl) FindObjectOfType(typeof(PuckControl));
		puck1 = puckStore.GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	void FixedUpdate () {
		Vector3 pos;
		Vector3 dir;

		if (puck1.position.x > -1 && puck1.position.x < rigidbody.position.x) { // If puck is on the AI's side, attack puck
			pos = puck1.position;
			dir = pos - rigidbody.position;
			Vector3 vel = dir.normalized * speed;

			float move = speed * Time.fixedDeltaTime;
			float distToTarget = dir.magnitude;

			if (move > distToTarget) {
				vel = vel * distToTarget / move;
			}

			rigidbody.velocity = vel;
		}

		else { // if puck isn't on AI's side, return to default position
			pos = defaultAIPos.position;
			dir = pos - rigidbody.position;
			Vector3 vel = dir.normalized * speed;

			float move = speed * Time.fixedDeltaTime;
			float distToTarget = dir.magnitude;

			if (move > distToTarget) {
				vel = vel * distToTarget / move;
			}

			rigidbody.velocity = vel;
		}
			
	}
}
