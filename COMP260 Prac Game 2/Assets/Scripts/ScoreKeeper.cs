﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreKeeper : MonoBehaviour {

	public int scorePerGoal = 1;
	private int[] score = new int[2];
	public Text[] scoreText;
	public Text winText;

	// Use this for initialization
	void Start () {
		GoalControl[] goals = FindObjectsOfType<GoalControl> ();

		for (int i = 0; i < goals.Length; i++) {
			goals [i].scoreGoalEvent += OnScoreGoal;
		}

		for (int i = 0; i < score.Length; i++) {
			score [i] = 0;
			scoreText [i].text = "0";
		}
	}

	public void OnScoreGoal(int player) {
		score [player] += scorePerGoal;
		if (score [player] == 10) {
			winText.text = "Player " + (player + 1) + " Wins!";
			Time.timeScale = 0;
		}
		scoreText [player].text = score [player].ToString ();
		Debug.Log ("Player " + (player + 1) + ": " + score[player]);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
