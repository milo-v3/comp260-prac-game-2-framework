﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]

public class GoalControl : MonoBehaviour {

	public AudioClip scoreClip;
	private AudioSource audio;

	public delegate void ScoreGoalHandler (int player);
	public ScoreGoalHandler scoreGoalEvent;
	public int player;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter (Collider collider) {
		Debug.Log ("Play Score Clip");
		audio.PlayOneShot (scoreClip);

		PuckControl puck = collider.gameObject.GetComponent<PuckControl> ();
		puck.ResetPosition ();

		if (scoreGoalEvent != null) {
				scoreGoalEvent (player);
		}
	}
}
