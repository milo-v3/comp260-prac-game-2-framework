﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class MovePaddle : MonoBehaviour {

	private Rigidbody rigidbody;
	public float speed;
	public string controlScheme = "mouse";
	public float force = 15f;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private Vector3 GetMousePosition() {
		// Create a ray from the camera passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		// fiundout where the ray intersects the XY plane
		Plane plane = new Plane(Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast (ray, out distance);
		return ray.GetPoint(distance);
	}

	void OnDrawGizmos() {
		// draw the mouse-ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine (Camera.main.transform.position, pos);
	}

	void FixedUpdate () {
		Vector3 pos;
		Vector3 dir;

		if (controlScheme == "mouse") {
			pos = GetMousePosition ();
			dir = pos - rigidbody.position;
			Vector3 vel = dir.normalized * speed;

			float move = speed * Time.fixedDeltaTime;
			float distToTarget = dir.magnitude;

			if (move > distToTarget) {
				vel = vel * distToTarget / move;
			}

			rigidbody.velocity = vel;
		}
		if (controlScheme == "keyboard") {
			float horiz = Input.GetAxis ("Horizontal");
			pos = rigidbody.position;
			if (horiz > 0 | horiz < 0) {
				pos.x = pos.x + horiz;
			}
				
			float vert = Input.GetAxis ("Vertical");
			if (vert > 0 | vert < 0) {
				pos.y = pos.y + vert;
			}

			dir = pos - rigidbody.position;
			rigidbody.AddForce (dir.normalized * force);
		}
	}
}